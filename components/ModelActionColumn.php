<?php

namespace oteixido\gui\components;

use yii\i18n\PhpMessageSource;
use yii\helpers\Html;
use Yii;

class ModelActionColumn extends \yii\grid\ActionColumn
{
    public $template = '{update}&nbsp;{delete}';
    public $contentOptions = [ 'style' => 'white-space: nowrap' ];
    public $actions = [];

    public function init()
    {
        parent::init();
        Yii::$app->i18n->translations['oteixido/gui'] = [
            'class' => PhpMessageSource::className(),
            'sourceLanguage' => 'ca-ES',
            'basePath' => '@app/vendor/oteixido/yii2-gui/messages'
        ];
        $defaultActions = [
            'view' => [
                'title' => Yii::t('oteixido/gui', 'Mostrar'),
                'icon' => 'glyphicon-eye-open',
            ],
            'update' => [
                'title' => Yii::t('oteixido/gui', 'Modificar'),
                'icon' => 'glyphicon-pencil',
            ],
            'delete' => [
                'title' => Yii::t('oteixido/gui', 'Esborrar'),
                'icon' => 'glyphicon-trash',
                'data' => [
                    'confirm' => Yii::t('oteixido/gui', 'Segur que vols esborrar aquest element?'),
                    'method' => 'post',
                ],
                'class' => 'btn-danger',
            ],
        ];
        $defaultOptions = [
            'title' => '',
            'icon' => 'glyphicon-exclamation-sign',
            'class' => 'btn-primary',
            'data' => [],
        ];
        $this->actions = array_merge($defaultActions, $this->actions);
        foreach($this->actions as $key => $value) {
            $options = array_merge($defaultOptions, $value);
            $this->buttons[$key] = function ($url) use ($options) {
                return Html::a('<span class="btn btn-sm '.$options['class'].' glyphicon '.$options['icon'].'"></span>',
                    $url,
                    [ 'title' => $options['title'], 'data-pjax' => '0', 'data' => $options['data'] ]
                );
            };
        }
    }
}
