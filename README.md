Yii2 gui
========
Yii2 gui

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist oteixido/yii2-gui "*"
```

or add

```
"oteixido/yii2-gui": "*"
```

to the require section of your `composer.json` file.
