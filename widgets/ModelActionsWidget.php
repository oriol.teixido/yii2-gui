<?php
namespace oteixido\gui\widgets;

use Yii;

use oteixido\gui\widgets\buttons\CreateButtonWidget;
use oteixido\gui\widgets\buttons\DeleteButtonWidget;

class ModelActionsWidget extends ActionsWidget
{
    public $model = null;

    public function defaults()
    {
        switch(Yii::$app->controller->action->id) {
            case 'update':
                return [ DeleteButtonWidget::widget(['model' => $this->model ]) ];
            case 'index':
                return [ CreateButtonWidget::widget() ];
        }
        return [];
    }
}
