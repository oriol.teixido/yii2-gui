<?php

namespace oteixido\gui\widgets;

use yii\i18n\PhpMessageSource;
use yii\base\Widget;
use Yii;

class ActionsWidget extends Widget
{
    public $actions = [];

    public function init()
    {
        parent::init();
        Yii::$app->i18n->translations['oteixido/gui'] = [
            'class' => PhpMessageSource::className(),
            'sourceLanguage' => 'ca-ES',
            'basePath' => '@app/vendor/oteixido/yii2-gui/messages'
        ];
        $this->actions = array_merge($this->defaults(), $this->actions);
    }

    public function run()
    {
        return '<div class="actions-form">'.implode ('', $this->actions).'</div>';
    }

    public function defaults()
    {
        return [];
    }
}
