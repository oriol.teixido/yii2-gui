<?php

namespace oteixido\gui\widgets\buttons;

use yii\base\Widget;
use yii\helpers\Html;
use yii\i18n\PhpMessageSource;

use Yii;

class SubmitButtonWidget extends Widget
{
    public $title = null;
    public $icon = null;
    public $style = null;
    public $name = null;
    public $value = null;
    public $htmlOptions = null;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        Yii::$app->i18n->translations['oteixido/gui'] = [
            'class' => PhpMessageSource::className(),
            'sourceLanguage' => 'ca-ES',
            'basePath' => '@app/vendor/oteixido/yii2-gui/messages'
        ];
        $this->config(array_merge(self::defaults(), $this->defaults()));
    }


    /**
     * Retorna els valors per fedecte de l'objecte.
     *
     * @return Array Valors per defecte de l'objecte
     */
    public function defaults()
    {
        return [
            'title' => Yii::t('oteixido/gui', 'Enviar'),
            'icon' => 'glyphicon-ok',
            'style' => 'success',
            'htmlOptions' => [],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $htmlOptions = array_merge([
            'class' => 'btn btn-'.$this->style,
        ], $this->htmlOptions);
        if (!empty($this->name)) {
            $htmlOptions['name'] = $this->name;
        }
        if (!empty($this->value)) {
            $htmlOptions['value'] = $this->value;
        }
        return Html::submitButton('<span class="glyphicon '.$this->icon.'"></span> '.$this->title, $htmlOptions);
    }

    /**
     * Assigna els valors per defecte en cas no tinguin cap valor assignat encara.
     */
    private function config($config)
    {
        foreach($config as $attribute => $value) {
            if ($this->$attribute === null) {
                $this->$attribute = $value;
            }
        }
    }
}
