<?php

namespace oteixido\gui\widgets\buttons;

use Yii;

class AddButtonWidget extends ButtonWidget
{
    public $foreignKey = null;

    /**
     * {@inheritdoc}
     */
    public function defaults()
    {
        return [
            'title' => Yii::t('oteixido/gui', 'Afegir'),
            'icon' => 'glyphicon-plus',
            'actionId' => 'create',
            'parameters' => $this->foreignKey,
        ];
    }
}
