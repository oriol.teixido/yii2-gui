<?php

namespace oteixido\gui\widgets\buttons;

use Yii;

class DeleteButtonWidget extends ButtonWidget
{
    public $model = null;

    /**
     * {@inheritdoc}
     */
    public function defaults()
    {
        return [
            'title' => Yii::t('oteixido/gui', 'Esborrar'),
            'icon' => 'glyphicon-trash',
            'actionId' => 'delete',
            'parameters' => $this->model->getPrimaryKey($asArray = true),
            'style' => 'danger',
            'htmlOptions' => [
                'data' => [
                    'confirm' => Yii::t('oteixido/gui', 'Segur que vols esborrar aquest element?'),
                    'method' => 'post',
                ],
            ],
        ];
    }
}
