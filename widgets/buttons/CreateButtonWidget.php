<?php

namespace oteixido\gui\widgets\buttons;

use Yii;

class CreateButtonWidget extends ButtonWidget
{
    /**
     * {@inheritdoc}
     */
    public function defaults()
    {
        return [
            'title' => Yii::t('oteixido/gui', 'Crear'),
            'icon' => 'glyphicon-file',
            'actionId' => 'create',
        ];
    }
}
