<?php

namespace oteixido\gui\widgets\buttons;

use Yii;

class UpdateButtonWidget extends ButtonWidget
{
    public $model = null;

    /**
     * {@inheritdoc}
     */
    public function defaults()
    {
        return [
            'title' => Yii::t('oteixido/gui', 'Modificar'),
            'icon' => 'glyphicon-pencil',
            'actionId' => 'update',
            'parameters' => $model->getPrimaryKey($asArray = true),
        ];
    }
}
