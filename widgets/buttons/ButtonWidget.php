<?php

namespace oteixido\gui\widgets\buttons;

use yii\base\Widget;
use yii\helpers\Html;
use yii\i18n\PhpMessageSource;
use Yii;

class ButtonWidget extends Widget
{
    public $title = null;
    public $actionId = null;
    public $moduleId = null;
    public $controllerId = null;
    public $parameters = null;
    public $icon = null;
    public $style = null;
    public $htmlOptions = null;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        Yii::$app->i18n->translations['oteixido/gui'] = [
            'class' => PhpMessageSource::className(),
            'sourceLanguage' => 'ca-ES',
            'basePath' => '@app/vendor/oteixido/yii2-gui/messages'
        ];
        $this->config(array_merge(self::defaults(), $this->defaults()));
    }

    /**
     * Retorna els valors per fedecte de l'objecte.
     *
     * @return Array Valors per defecte de l'objecte
     */
    public function defaults()
    {
        return [
            'title' => '',
            'icon' => 'glyphicon-ok',
            'style' => 'primary',
            'htmlOptions' => [],
            'actionId' => Yii::$app->controller->action->id,
            'controllerId' => Yii::$app->controller->id,
            'moduleId' => Yii::$app->controller->module->id,
            'parameters' => [],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $this->htmlOptions['title'] = $this->title;
        $text = '<span class="btn btn-'.$this->style.'"><span class="glyphicon '.$this->icon.'"></span> '.$this->title.'</span>';
        $action = array_merge([ $this->getRoute() ], $this->parameters);
        return Html::a($text, $action, $this->htmlOptions);
    }

    /**
     * Assigna els valors per defecte en cas no tinguin cap valor assignat encara.
     */
    private function config($config)
    {
        foreach($config as $attribute => $value) {
            if ($this->$attribute === null) {
                $this->$attribute = $value;
            }
        }
    }

    /**
     * Retorna la ruta de l'acció (module/controller/action?parameters)
     */
    private function getRoute() {
        return ($this->moduleId == Yii::$app->id ? '' : '/' . $this->moduleId) . '/' . $this->controllerId . '/' . $this->actionId;
    }
}
